<?php

/* profiles/social/modules/social_features/social_user/templates/account-header-links.html.twig */
class __TwigTemplate_e93651914e50269f6147dd743314af165e8e3d3c84520c4a444ea34bc43ac9ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("for" => 17, "if" => 19);
        $filters = array("without" => 17);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('for', 'if'),
                array('without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 15
        echo "
<ul class=\"nav navbar-nav navbar-right\">
";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_without((isset($context["links"]) ? $context["links"] : null), "search_block"));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 18
            echo "    <li class=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "classes", array()), "html", null, true));
            echo "\">
        <a href=\"";
            // line 19
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true));
            echo "\" class=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "link_classes", array()), "html", null, true));
            echo "\" ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "link_attributes", array()), "html", null, true));
            echo " ";
            if ($this->getAttribute($context["item"], "title", array())) {
                echo " title=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true));
                echo "\" ";
            }
            echo ">
          ";
            // line 20
            if ($this->getAttribute($context["item"], "icon_image", array())) {
                // line 21
                echo "            ";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "icon_image", array()), "html", null, true));
                echo "
          ";
            } elseif ($this->getAttribute(            // line 22
$context["item"], "icon_classes", array())) {
                // line 23
                echo "            <svg class=\"icon-white icon-medium pull-left ";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "icon_classes", array()), "html", null, true));
                echo "\">
              <use xlink:href=\"#";
                // line 24
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "icon_classes", array()), "html", null, true));
                echo "\" />
            </svg>
          ";
            }
            // line 27
            echo "          <span class=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "title_classes", array()), "html", null, true));
            echo "\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "label", array()), "html", null, true));
            echo "</span>
        </a>
        ";
            // line 29
            if ($this->getAttribute($context["item"], "below", array())) {
                // line 30
                echo "            ";
                if (twig_test_iterable($this->getAttribute($context["item"], "below", array()))) {
                    // line 31
                    echo "              <ul class=\"dropdown-menu\">
                ";
                    // line 32
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "below", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 33
                        echo "                  ";
                        if ($this->getAttribute($context["item"], "url", array())) {
                            // line 34
                            echo "                    <li class=\"";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "classes", array()), "html", null, true));
                            echo "\">
                      <a href=\"";
                            // line 35
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true));
                            echo "\" class=\"";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "link_classes", array()), "html", null, true));
                            echo "\" ";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "link_attributes", array()), "html", null, true));
                            echo " ";
                            if ($this->getAttribute($context["item"], "title", array())) {
                                echo " title=\"";
                                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true));
                                echo "\" ";
                            }
                            echo ">
                        <span class=\"";
                            // line 36
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "title_classes", array()), "html", null, true));
                            echo "\">";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "label", array()), "html", null, true));
                            echo "</span>
                      </a>
                    </li>
                  ";
                        } elseif ($this->getAttribute(                        // line 39
$context["item"], "divider", array())) {
                            // line 40
                            echo "                    <li class=\"";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "classes", array()), "html", null, true));
                            echo "\"></li>
                  ";
                        } else {
                            // line 42
                            echo "                    <li class=\"";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "classes", array()), "html", null, true));
                            echo "\" ";
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "attributes", array()), "html", null, true));
                            echo ">
                      ";
                            // line 43
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "tagline", array()), "html", null, true));
                            echo "
                      <strong class=\"text-truncate\"> ";
                            // line 44
                            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "object", array()), "html", null, true));
                            echo " </strong>
                    </li>
                  ";
                        }
                        // line 47
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 48
                    echo "              </ul>
            ";
                } else {
                    // line 50
                    echo "                <ul class=\"dropdown-menu\">
                    ";
                    // line 51
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "below", array()), "html", null, true));
                    echo "
                </ul>
            ";
                }
                // line 54
                echo "        ";
            }
            // line 55
            echo "    </li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "profiles/social/modules/social_features/social_user/templates/account-header-links.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 57,  187 => 55,  184 => 54,  178 => 51,  175 => 50,  171 => 48,  165 => 47,  159 => 44,  155 => 43,  148 => 42,  142 => 40,  140 => 39,  132 => 36,  118 => 35,  113 => 34,  110 => 33,  106 => 32,  103 => 31,  100 => 30,  98 => 29,  90 => 27,  84 => 24,  79 => 23,  77 => 22,  72 => 21,  70 => 20,  56 => 19,  51 => 18,  47 => 17,  43 => 15,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display a menu.*/
/*  **/
/*  * Available variables:*/
/*  * Links*/
/*  *  Link:*/
/*  *  - classes*/
/*  *  - label*/
/*  *  - url*/
/*  * @ingroup templates*/
/*  *//* */
/* #}*/
/* */
/* <ul class="nav navbar-nav navbar-right">*/
/* {% for item in links|without('search_block') %}*/
/*     <li class="{{ item.classes }}">*/
/*         <a href="{{ item.url }}" class="{{ item.link_classes }}" {{ item.link_attributes }} {% if item.title %} title="{{ item.title }}" {% endif %}>*/
/*           {% if item.icon_image %}*/
/*             {{ item.icon_image }}*/
/*           {% elseif item.icon_classes %}*/
/*             <svg class="icon-white icon-medium pull-left {{ item.icon_classes }}">*/
/*               <use xlink:href="#{{ item.icon_classes }}" />*/
/*             </svg>*/
/*           {% endif %}*/
/*           <span class="{{item.title_classes}}">{{ item.label }}</span>*/
/*         </a>*/
/*         {% if item.below %}*/
/*             {% if item.below is iterable %}*/
/*               <ul class="dropdown-menu">*/
/*                 {% for item in item.below %}*/
/*                   {% if item.url %}*/
/*                     <li class="{{ item.classes }}">*/
/*                       <a href="{{ item.url }}" class="{{ item.link_classes }}" {{ item.link_attributes }} {% if item.title %} title="{{ item.title }}" {% endif %}>*/
/*                         <span class="{{item.title_classes}}">{{ item.label }}</span>*/
/*                       </a>*/
/*                     </li>*/
/*                   {% elseif item.divider %}*/
/*                     <li class="{{ item.classes }}"></li>*/
/*                   {% else %}*/
/*                     <li class="{{ item.classes }}" {{ item.attributes }}>*/
/*                       {{ item.tagline }}*/
/*                       <strong class="text-truncate"> {{ item.object }} </strong>*/
/*                     </li>*/
/*                   {% endif %}*/
/*                 {% endfor %}*/
/*               </ul>*/
/*             {% else %}*/
/*                 <ul class="dropdown-menu">*/
/*                     {{ item.below }}*/
/*                 </ul>*/
/*             {% endif %}*/
/*         {% endif %}*/
/*     </li>*/
/* {% endfor %}*/
/* </ul>*/
/* */
