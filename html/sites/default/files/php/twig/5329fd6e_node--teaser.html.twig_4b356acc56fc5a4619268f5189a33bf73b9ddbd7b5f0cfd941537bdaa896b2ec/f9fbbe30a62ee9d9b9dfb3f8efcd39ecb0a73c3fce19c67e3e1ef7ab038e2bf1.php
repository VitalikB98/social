<?php

/* node--teaser.html.twig */
class __TwigTemplate_87d754ce0852e95050aa68324b4634b3f0ef846ebcdb209f8d339044f4500f61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'card_teaser_type' => array($this, 'block_card_teaser_type'),
            'card_image' => array($this, 'block_card_image'),
            'card_body' => array($this, 'block_card_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 72, "if" => 88, "block" => 89, "trans" => 131);
        $filters = array();
        $functions = array("attach_library" => 69);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if', 'block', 'trans'),
                array(),
                array('attach_library')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 68
        echo "
";
        // line 69
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->attachLibrary("socialbase/teaser"), "html", null, true));
        echo "

";
        // line 72
        $context["classes"] = array(0 => (((        // line 73
(isset($context["view_mode"]) ? $context["view_mode"] : null) == "teaser")) ? ("card") : ("")), 1 => "card--teaser", 2 => "card-with-actionbar", 3 => (((        // line 76
(isset($context["view_mode"]) ? $context["view_mode"] : null) == "activity")) ? ("card--teaser-stream") : ("")), 4 => (((        // line 77
(isset($context["view_mode"]) ? $context["view_mode"] : null) == "activity_comment")) ? ("card--teaser-stream") : ("")), 5 => (($this->getAttribute(        // line 78
(isset($context["node"]) ? $context["node"] : null), "isPromoted", array(), "method")) ? ("promoted") : ("")), 6 => (($this->getAttribute(        // line 79
(isset($context["node"]) ? $context["node"] : null), "isSticky", array(), "method")) ? ("sticky") : ("")), 7 => (( !$this->getAttribute(        // line 80
(isset($context["node"]) ? $context["node"] : null), "isPublished", array(), "method")) ? ("unpublished") : ("")));
        // line 83
        echo "
<div ";
        // line 84
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method"), "html", null, true));
        echo ">

  <div class='card-image'>

    ";
        // line 88
        if (((isset($context["view_mode"]) ? $context["view_mode"] : null) == "teaser")) {
            // line 89
            echo "      ";
            $this->displayBlock('card_teaser_type', $context, $blocks);
            // line 90
            echo "    ";
        }
        // line 91
        echo "
    ";
        // line 92
        $this->displayBlock('card_image', $context, $blocks);
        // line 93
        echo "
    ";
        // line 94
        if ((isset($context["status_label"]) ? $context["status_label"] : null)) {
            // line 95
            echo "      <div class=\"status\">
        ";
            // line 96
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["status_label"]) ? $context["status_label"] : null), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 99
        echo "
  </div>

  <div class='card-body'>
    ";
        // line 103
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_prefix"]) ? $context["title_prefix"] : null), "html", null, true));
        echo "
    ";
        // line 104
        if ( !(isset($context["page"]) ? $context["page"] : null)) {
            // line 105
            echo "      <h4";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_attributes"]) ? $context["title_attributes"] : null), "html", null, true));
            echo " class=\"card-title\">
          <a href=\"";
            // line 106
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true));
            echo "\" rel=\"bookmark\">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true));
            echo "</a>
      </h4>
    ";
        }
        // line 109
        echo "    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_suffix"]) ? $context["title_suffix"] : null), "html", null, true));
        echo "

    ";
        // line 111
        $this->displayBlock('card_body', $context, $blocks);
        // line 114
        echo "
    <div class=\"card-actionbar\">
      <div class=\"actionbar\">

        ";
        // line 118
        if (((isset($context["comment_count"]) ? $context["comment_count"] : null) > 0)) {
            // line 119
            echo "          <a href=\"";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true));
            echo "#section-comments\" class=\"badge badge--comment-count\">
            <label>
              <svg class=\"icon-small icon-badge\">
                <use xlink:href=\"#icon-comment\"></use>
              </svg>
              <span class=\"badge--comment-count__number\">";
            // line 124
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["comment_count"]) ? $context["comment_count"] : null), "html", null, true));
            echo "</span>
            </label>
          </a>
        ";
        }
        // line 128
        echo "
        ";
        // line 129
        if (((isset($context["visibility_icon"]) ? $context["visibility_icon"] : null) && (isset($context["visibility_label"]) ? $context["visibility_label"] : null))) {
            // line 130
            echo "          <div class=\"badge badge--toggle\">
            <label title=\"";
            // line 131
            echo t("The visibility of this content is set to", array());
            echo " ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["visibility_label"]) ? $context["visibility_label"] : null), "html", null, true));
            echo "\">
              <input type=\"checkbox\">
              <svg class=\"icon-gray icon-badge-toggle\">
                <use xlink:href=\"#icon-";
            // line 134
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["visibility_icon"]) ? $context["visibility_icon"] : null), "html", null, true));
            echo "\"></use>
              </svg>
              <span class=\"badge--toggle__label\">";
            // line 136
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["visibility_label"]) ? $context["visibility_label"] : null), "html", null, true));
            echo "</span>
            </label>
          </div>
        ";
        }
        // line 140
        echo "
        ";
        // line 141
        if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "enrolled", array())) {
            // line 142
            echo "          <span class=\"label label-default\">";
            // line 143
            echo t("Enrolled", array());
            // line 144
            echo "</span>
        ";
        }
        // line 146
        echo "      </div>
      ";
        // line 147
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "links", array()), "html", null, true));
        echo "
    </div>

  </div>

</div>

";
        // line 154
        if ((((isset($context["view_mode"]) ? $context["view_mode"] : null) == "activity") && $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_book_comments", array()))) {
            // line 155
            echo "  ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_book_comments", array()), "html", null, true));
            echo "
";
        }
    }

    // line 89
    public function block_card_teaser_type($context, array $blocks = array())
    {
        echo " ";
    }

    // line 92
    public function block_card_image($context, array $blocks = array())
    {
        echo " ";
    }

    // line 111
    public function block_card_body($context, array $blocks = array())
    {
        // line 112
        echo "      ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "node--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 112,  232 => 111,  226 => 92,  220 => 89,  212 => 155,  210 => 154,  200 => 147,  197 => 146,  193 => 144,  191 => 143,  189 => 142,  187 => 141,  184 => 140,  177 => 136,  172 => 134,  164 => 131,  161 => 130,  159 => 129,  156 => 128,  149 => 124,  140 => 119,  138 => 118,  132 => 114,  130 => 111,  124 => 109,  116 => 106,  111 => 105,  109 => 104,  105 => 103,  99 => 99,  93 => 96,  90 => 95,  88 => 94,  85 => 93,  83 => 92,  80 => 91,  77 => 90,  74 => 89,  72 => 88,  65 => 84,  62 => 83,  60 => 80,  59 => 79,  58 => 78,  57 => 77,  56 => 76,  55 => 73,  54 => 72,  49 => 69,  46 => 68,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display a node.*/
/*  **/
/*  * Available variables:*/
/*  * - node: The node entity with limited access to object properties and methods.*/
/*      Only "getter" methods (method names starting with "get", "has", or "is")*/
/*      and a few common methods such as "id" and "label" are available. Calling*/
/*      other methods (such as node.delete) will result in an exception.*/
/*  * - label: The title of the node.*/
/*  * - content: All node items. Use {{ content }} to print them all,*/
/*  *   or print a subset such as {{ content.field_example }}. Use*/
/*  *   {{ content|without('field_example') }} to temporarily suppress the printing*/
/*  *   of a given child element.*/
/*  * - author_picture: The node author user entity, rendered using the "compact"*/
/*  *   view mode.*/
/*  * - metadata: Metadata for this node.*/
/*  * - date: Themed creation date field.*/
/*  * - author_name: Themed author name field.*/
/*  * - url: Direct URL of the current node.*/
/*  * - display_submitted: Whether submission information should be displayed.*/
/*  * - attributes: HTML attributes for the containing element.*/
/*  *   The attributes.class element may contain one or more of the following*/
/*  *   classes:*/
/*  *   - node: The current template type (also known as a "theming hook").*/
/*  *   - node--type-[type]: The current node type. For example, if the node is an*/
/*  *     "Article" it would result in "node--type-article". Note that the machine*/
/*  *     name will often be in a short form of the human readable label.*/
/*  *   - node--view-mode-[view_mode]: The View Mode of the node; for example, a*/
/*  *     teaser would result in: "node--view-mode-teaser", and*/
/*  *     full: "node--view-mode-full".*/
/*  *   The following are controlled through the node publishing options.*/
/*  *   - node--promoted: Appears on nodes promoted to the front page.*/
/*  *   - node--sticky: Appears on nodes ordered above other non-sticky nodes in*/
/*  *     teaser listings.*/
/*  *   - node--unpublished: Appears on unpublished nodes visible only to site*/
/*  *     admins.*/
/*  * - title_attributes: Same as attributes, except applied to the main title*/
/*  *   tag that appears in the template.*/
/*  * - content_attributes: Same as attributes, except applied to the main*/
/*  *   content tag that appears in the template.*/
/*  * - author_attributes: Same as attributes, except applied to the author of*/
/*  *   the node tag that appears in the template.*/
/*  * - title_prefix: Additional output populated by modules, intended to be*/
/*  *   displayed in front of the main title tag that appears in the template.*/
/*  * - title_suffix: Additional output populated by modules, intended to be*/
/*  *   displayed after the main title tag that appears in the template.*/
/*  * - view_mode: View mode; for example, "teaser" or "full".*/
/*  * - teaser: Flag for the teaser state. Will be true if view_mode is 'teaser'.*/
/*  * - page: Flag for the full page state. Will be true if view_mode is 'full'.*/
/*  * - readmore: Flag for more state. Will be true if the teaser content of the*/
/*  *   node cannot hold the main body content.*/
/*  * - logged_in: Flag for authenticated user status. Will be true when the*/
/*  *   current user is a logged-in member.*/
/*  * - is_admin: Flag for admin user status. Will be true when the current user*/
/*  *   is an administrator.*/
/*  **/
/*  * @see template_preprocess_node()*/
/*  **/
/*  * @todo Remove the id attribute (or make it a class), because if that gets*/
/*  *   rendered twice on a page this is invalid CSS for example: two lists*/
/*  *   in different view modes.*/
/*  **/
/*  * @ingroup themeable*/
/*  *//* */
/* #}*/
/* */
/* {{ attach_library('socialbase/teaser')}}*/
/* */
/* {%*/
/*   set classes = [*/
/*     view_mode == 'teaser' ? 'card',*/
/*     'card--teaser',*/
/*     'card-with-actionbar',*/
/*     view_mode == 'activity' ? 'card--teaser-stream',*/
/*     view_mode == 'activity_comment' ? 'card--teaser-stream',*/
/*     node.isPromoted() ? 'promoted',*/
/*     node.isSticky() ? 'sticky',*/
/*     not node.isPublished() ? 'unpublished',*/
/*   ]*/
/* %}*/
/* */
/* <div {{ attributes.addClass(classes) }}>*/
/* */
/*   <div class='card-image'>*/
/* */
/*     {% if view_mode == 'teaser' %}*/
/*       {% block card_teaser_type %} {% endblock %}*/
/*     {% endif %}*/
/* */
/*     {% block card_image %} {% endblock %}*/
/* */
/*     {% if status_label %}*/
/*       <div class="status">*/
/*         {{ status_label }}*/
/*       </div>*/
/*     {% endif %}*/
/* */
/*   </div>*/
/* */
/*   <div class='card-body'>*/
/*     {{ title_prefix }}*/
/*     {% if not page %}*/
/*       <h4{{ title_attributes }} class="card-title">*/
/*           <a href="{{ url }}" rel="bookmark">{{ label }}</a>*/
/*       </h4>*/
/*     {% endif %}*/
/*     {{ title_suffix }}*/
/* */
/*     {% block card_body %}*/
/*       {{ content }}*/
/*     {% endblock %}*/
/* */
/*     <div class="card-actionbar">*/
/*       <div class="actionbar">*/
/* */
/*         {% if comment_count > 0 %}*/
/*           <a href="{{ url }}#section-comments" class="badge badge--comment-count">*/
/*             <label>*/
/*               <svg class="icon-small icon-badge">*/
/*                 <use xlink:href="#icon-comment"></use>*/
/*               </svg>*/
/*               <span class="badge--comment-count__number">{{ comment_count }}</span>*/
/*             </label>*/
/*           </a>*/
/*         {% endif %}*/
/* */
/*         {% if visibility_icon and visibility_label %}*/
/*           <div class="badge badge--toggle">*/
/*             <label title="{% trans %}The visibility of this content is set to {% endtrans %} {{ visibility_label }}">*/
/*               <input type="checkbox">*/
/*               <svg class="icon-gray icon-badge-toggle">*/
/*                 <use xlink:href="#icon-{{ visibility_icon }}"></use>*/
/*               </svg>*/
/*               <span class="badge--toggle__label">{{ visibility_label }}</span>*/
/*             </label>*/
/*           </div>*/
/*         {% endif %}*/
/* */
/*         {% if content.enrolled %}*/
/*           <span class="label label-default">*/
/*             {%- trans -%} Enrolled {%- endtrans -%}*/
/*           </span>*/
/*         {% endif %}*/
/*       </div>*/
/*       {{ content.links }}*/
/*     </div>*/
/* */
/*   </div>*/
/* */
/* </div>*/
/* */
/* {% if view_mode == 'activity' and content.field_book_comments %}*/
/*   {{ content.field_book_comments }}*/
/* {% endif %}*/
/* */
