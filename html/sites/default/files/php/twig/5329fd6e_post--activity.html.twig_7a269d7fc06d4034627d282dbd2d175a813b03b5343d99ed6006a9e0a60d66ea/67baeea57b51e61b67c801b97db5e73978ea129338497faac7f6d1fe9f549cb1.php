<?php

/* profiles/social/themes/socialbase/templates/post/post--activity.html.twig */
class __TwigTemplate_43b0692e2150552f629da0d5f340b553d709aac5c39ff416ed13eacd56663037 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 20, "if" => 27);
        $filters = array("render" => 27);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if'),
                array('render'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "  ";
        // line 18
        echo "
";
        // line 20
        $context["classes"] = array(0 => "margin-top-s", 1 => ((        // line 22
(isset($context["logged_in"]) ? $context["logged_in"] : null)) ? ("margin-bottom-m") : ("")));
        // line 25
        echo "

";
        // line 27
        if ($this->env->getExtension('drupal_core')->renderVar($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "links", array()))) {
            // line 28
            echo "  ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "links", array()), "html", null, true));
            echo "
";
        }
        // line 30
        echo "
<div";
        // line 31
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method"), "html", null, true));
        echo ">

  ";
        // line 33
        if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_post", array())) {
            // line 34
            echo "    ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_post", array()), "html", null, true));
            echo "
  ";
        }
        // line 36
        echo "
</div>

";
        // line 39
        if ((isset($context["logged_in"]) ? $context["logged_in"] : null)) {
            // line 40
            echo "  <div class=\"card-nested-section\">
    ";
            // line 41
            if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_post_comments", array())) {
                // line 42
                echo "      ";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_post_comments", array()), "html", null, true));
                echo "
    ";
            }
            // line 44
            echo "  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "profiles/social/themes/socialbase/templates/post/post--activity.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 44,  91 => 42,  89 => 41,  86 => 40,  84 => 39,  79 => 36,  73 => 34,  71 => 33,  66 => 31,  63 => 30,  57 => 28,  55 => 27,  51 => 25,  49 => 22,  48 => 20,  45 => 18,  43 => 1,);
    }
}
/*   {#*/
/*   /***/
/*   * @file post.html.twig*/
/*   * Default theme implementation to present Post data.*/
/*   **/
/*   * This template is used when viewing Post pages.*/
/*   **/
/*   **/
/*   * Available variables:*/
/*   * - content: A list of content items. Use 'content' to print all content, or*/
/*   * - attributes: HTML attributes for the container element.*/
/*   **/
/*   * @see template_preprocess_post()*/
/*   **/
/*   * @ingroup themeable*/
/*   *//* */
/*   #}*/
/* */
/* {%*/
/*   set classes = [*/
/*     'margin-top-s',*/
/*     logged_in ? 'margin-bottom-m',*/
/*   ]*/
/* %}*/
/* */
/* */
/* {% if content.links|render %}*/
/*   {{ content.links }}*/
/* {% endif %}*/
/* */
/* <div{{ attributes.addClass(classes) }}>*/
/* */
/*   {% if content.field_post %}*/
/*     {{ content.field_post }}*/
/*   {% endif %}*/
/* */
/* </div>*/
/* */
/* {% if logged_in %}*/
/*   <div class="card-nested-section">*/
/*     {% if content.field_post_comments %}*/
/*       {{ content.field_post_comments }}*/
/*     {% endif %}*/
/*   </div>*/
/* {% endif %}*/
/* */
