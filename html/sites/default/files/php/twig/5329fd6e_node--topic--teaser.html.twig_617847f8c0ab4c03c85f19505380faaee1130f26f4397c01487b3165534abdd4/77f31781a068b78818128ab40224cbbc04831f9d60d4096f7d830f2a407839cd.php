<?php

/* node--topic--teaser.html.twig */
class __TwigTemplate_b9e0942c5fa826192ee9e32e5cf08e1fc614dee0447c3b60450b2a4608cd3a44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("node--teaser.html.twig", "node--topic--teaser.html.twig", 1);
        $this->blocks = array(
            'card_image' => array($this, 'block_card_image'),
            'card_teaser_type' => array($this, 'block_card_teaser_type'),
            'card_body' => array($this, 'block_card_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("embed" => 8, "if" => 30);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('embed', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_card_image($context, array $blocks = array())
    {
        // line 4
        echo "  ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "field_topic_image", array()), "html", null, true));
        echo "
";
    }

    // line 7
    public function block_card_teaser_type($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        $this->loadTemplate("node--topic--teaser.html.twig", "node--topic--teaser.html.twig", 8, "853278420")->display($context);
    }

    // line 13
    public function block_card_body($context, array $blocks = array())
    {
        // line 14
        echo "
  ";
        // line 15
        $this->loadTemplate("node--topic--teaser.html.twig", "node--topic--teaser.html.twig", 15, "1886073564")->display($context);
        // line 24
        echo "
  ";
        // line 25
        $this->loadTemplate("node--topic--teaser.html.twig", "node--topic--teaser.html.twig", 25, "1150409055")->display($context);
        // line 29
        echo "
  ";
        // line 30
        if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "group_name", array())) {
            // line 31
            echo "    ";
            $this->loadTemplate("node--topic--teaser.html.twig", "node--topic--teaser.html.twig", 31, "1752423159")->display($context);
            // line 35
            echo "  ";
        }
        // line 36
        echo "
";
    }

    public function getTemplateName()
    {
        return "node--topic--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 36,  93 => 35,  90 => 31,  88 => 30,  85 => 29,  83 => 25,  80 => 24,  78 => 15,  75 => 14,  72 => 13,  67 => 8,  64 => 7,  57 => 4,  54 => 3,  11 => 1,);
    }
}


/* node--topic--teaser.html.twig */
class __TwigTemplate_b9e0942c5fa826192ee9e32e5cf08e1fc614dee0447c3b60450b2a4608cd3a44_853278420 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 8
        $this->parent = $this->loadTemplate("node--teaser__type.html.twig", "node--topic--teaser.html.twig", 8);
        $this->blocks = array(
            'card_teaser_type' => array($this, 'block_card_teaser_type'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__type.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_card_teaser_type($context, array $blocks = array())
    {
        echo "topic";
    }

    public function getTemplateName()
    {
        return "node--topic--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 9,  125 => 8,  96 => 36,  93 => 35,  90 => 31,  88 => 30,  85 => 29,  83 => 25,  80 => 24,  78 => 15,  75 => 14,  72 => 13,  67 => 8,  64 => 7,  57 => 4,  54 => 3,  11 => 1,);
    }
}


/* node--topic--teaser.html.twig */
class __TwigTemplate_b9e0942c5fa826192ee9e32e5cf08e1fc614dee0447c3b60450b2a4608cd3a44_1886073564 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 15
        $this->parent = $this->loadTemplate("node--teaser__field.html.twig", "node--topic--teaser.html.twig", 15);
        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 16
    public function block_field_icon($context, array $blocks = array())
    {
        echo "account_circle";
    }

    // line 17
    public function block_field_value($context, array $blocks = array())
    {
        // line 18
        echo "      <div class=\"teaser-published\">
        <div class=\"teaser-published__author\"> ";
        // line 19
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["author_name"]) ? $context["author_name"] : null), "html", null, true));
        echo " </div>
        <div class=\"teaser-published__date\"> &bullet; ";
        // line 20
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true));
        echo " </div>
      </div>";
    }

    public function getTemplateName()
    {
        return "node--topic--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 20,  250 => 19,  247 => 18,  244 => 17,  238 => 16,  196 => 15,  166 => 9,  125 => 8,  96 => 36,  93 => 35,  90 => 31,  88 => 30,  85 => 29,  83 => 25,  80 => 24,  78 => 15,  75 => 14,  72 => 13,  67 => 8,  64 => 7,  57 => 4,  54 => 3,  11 => 1,);
    }
}


/* node--topic--teaser.html.twig */
class __TwigTemplate_b9e0942c5fa826192ee9e32e5cf08e1fc614dee0447c3b60450b2a4608cd3a44_1150409055 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 25
        $this->parent = $this->loadTemplate("node--teaser__field.html.twig", "node--topic--teaser.html.twig", 25);
        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 26
    public function block_field_icon($context, array $blocks = array())
    {
        echo "label";
    }

    // line 27
    public function block_field_value($context, array $blocks = array())
    {
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["topic_type"]) ? $context["topic_type"] : null), "html", null, true));
    }

    public function getTemplateName()
    {
        return "node--topic--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  332 => 27,  326 => 26,  284 => 25,  254 => 20,  250 => 19,  247 => 18,  244 => 17,  238 => 16,  196 => 15,  166 => 9,  125 => 8,  96 => 36,  93 => 35,  90 => 31,  88 => 30,  85 => 29,  83 => 25,  80 => 24,  78 => 15,  75 => 14,  72 => 13,  67 => 8,  64 => 7,  57 => 4,  54 => 3,  11 => 1,);
    }
}


/* node--topic--teaser.html.twig */
class __TwigTemplate_b9e0942c5fa826192ee9e32e5cf08e1fc614dee0447c3b60450b2a4608cd3a44_1752423159 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 31
        $this->parent = $this->loadTemplate("node--teaser__field.html.twig", "node--topic--teaser.html.twig", 31);
        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 32
    public function block_field_icon($context, array $blocks = array())
    {
        echo "group";
    }

    // line 33
    public function block_field_value($context, array $blocks = array())
    {
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "group_name", array()), "html", null, true));
    }

    public function getTemplateName()
    {
        return "node--topic--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  410 => 33,  404 => 32,  362 => 31,  332 => 27,  326 => 26,  284 => 25,  254 => 20,  250 => 19,  247 => 18,  244 => 17,  238 => 16,  196 => 15,  166 => 9,  125 => 8,  96 => 36,  93 => 35,  90 => 31,  88 => 30,  85 => 29,  83 => 25,  80 => 24,  78 => 15,  75 => 14,  72 => 13,  67 => 8,  64 => 7,  57 => 4,  54 => 3,  11 => 1,);
    }
}
/* {% extends "node--teaser.html.twig" %}*/
/* */
/* {% block card_image %}*/
/*   {{ content.field_topic_image }}*/
/* {% endblock %}*/
/* */
/* {% block card_teaser_type %}*/
/*   {% embed "node--teaser__type.html.twig" %}*/
/*     {%- block card_teaser_type -%} topic {%- endblock -%}*/
/*   {% endembed %}*/
/* {% endblock %}*/
/* */
/* {% block card_body %}*/
/* */
/*   {% embed "node--teaser__field.html.twig" %}*/
/*     {%- block field_icon -%} account_circle {%- endblock -%}*/
/*     {%- block field_value %}*/
/*       <div class="teaser-published">*/
/*         <div class="teaser-published__author"> {{ author_name }} </div>*/
/*         <div class="teaser-published__date"> &bullet; {{ date }} </div>*/
/*       </div>*/
/*     {%- endblock -%}*/
/*   {% endembed %}*/
/* */
/*   {% embed "node--teaser__field.html.twig" %}*/
/*     {%- block field_icon -%} label {%- endblock -%}*/
/*     {%- block field_value -%} {{ topic_type }} {%- endblock -%}*/
/*   {% endembed %}*/
/* */
/*   {% if content.group_name %}*/
/*     {% embed "node--teaser__field.html.twig" %}*/
/*       {%- block field_icon -%} group {%- endblock -%}*/
/*       {%- block field_value -%} {{ content.group_name }} {%- endblock -%}*/
/*     {% endembed %}*/
/*   {% endif %}*/
/* */
/* {% endblock %}*/
/* */
