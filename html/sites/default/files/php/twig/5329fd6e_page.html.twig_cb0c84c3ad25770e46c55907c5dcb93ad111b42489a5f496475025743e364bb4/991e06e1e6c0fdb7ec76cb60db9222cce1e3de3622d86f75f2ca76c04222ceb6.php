<?php

/* profiles/social/themes/socialbase/templates/layout/page.html.twig */
class __TwigTemplate_2740e5469614b0632a6c9e1ba924000d35686aedabe2555ec6db9d84b098cbcb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 37, "if" => 49, "block" => 86);
        $filters = array("raw" => 120);
        $functions = array("attach_library" => 93);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('raw'),
                array('attach_library')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 35
        echo "
";
        // line 37
        $context["main_content_classes"] = array(0 => "region--main-content");
        // line 41
        echo "
<nav class=\"navbar navbar-default navbar-fixed-top\" role=\"banner\">
  <div class=\"container\">";
        // line 43
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "</div>
</nav>

<main id=\"content\" class=\"main-container\" role=\"main\">
  ";
        // line 47
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "breadcrumb", array()), "html", null, true));
        echo "

  ";
        // line 49
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "hero", array()) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "secondary_navigation", array()))) {
            // line 50
            echo "    <div class=\"container hero-container\">

      ";
            // line 52
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "hero", array()), "html", null, true));
            echo "

      ";
            // line 54
            if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "secondary_navigation", array())) {
                // line 55
                echo "        <div class=\"brand-secondary z-depth-1\">
          ";
                // line 56
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "secondary_navigation", array()), "html", null, true));
                echo "
        </div>
      ";
            }
            // line 59
            echo "
    </div>
  ";
        }
        // line 62
        echo "
  <a id=\"main-content\" tabindex=\"-1\"></a>

  <div class=\"container\">

    ";
        // line 67
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_top", array())) {
            // line 68
            echo "      <div class=\"margin-top-l\">
        ";
            // line 69
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_top", array()), "html", null, true));
            echo "
      </div>
    ";
        }
        // line 72
        echo "
    <div class=\"row margin-top-l\">

      ";
        // line 75
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "complementary_top", array()) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "complementary_bottom", array()))) {
            // line 76
            echo "        <aside class=\"region--complementary complementary-top\" role=\"complementary\">
          ";
            // line 77
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "complementary_top", array()), "html", null, true));
            echo "
        </aside>
      ";
        }
        // line 80
        echo "
      <div";
        // line 81
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["main_content_classes"]) ? $context["main_content_classes"] : null)), "method"), "html", null, true));
        echo "\">
        ";
        // line 82
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array()) && (isset($context["display_page_title"]) ? $context["display_page_title"] : null))) {
            // line 83
            echo "          ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array()), "html", null, true));
            echo "
        ";
        }
        // line 85
        echo "
        ";
        // line 86
        $this->displayBlock('content', $context, $blocks);
        // line 89
        echo "
      </div>

      ";
        // line 92
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "complementary_bottom", array())) {
            // line 93
            echo "      ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->attachLibrary("socialbase/responsive-dom"), "html", null, true));
            echo "
        <div role=\"complementary\" class=\"region--complementary\">
          <div class=\"complementary-bottom\">
            ";
            // line 96
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "complementary_bottom", array()), "html", null, true));
            echo "
          </div>
        </div>
      ";
        }
        // line 100
        echo "
      ";
        // line 101
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_bottom", array())) {
            // line 102
            echo "        <div class=\"row margin-top-sm\">
          ";
            // line 103
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_bottom", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 106
        echo "
    </div>
  </div>

</main>

";
        // line 112
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array())) {
            // line 113
            echo "  <footer class=\"site-footer\" role=\"contentinfo\">
    <div class=\"container clearfix\">
      ";
            // line 115
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
            echo "
    </div>
  </footer>
";
        }
        // line 119
        echo "
";
        // line 120
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar((isset($context["svg_icons"]) ? $context["svg_icons"] : null)));
        echo "
";
    }

    // line 86
    public function block_content($context, array $blocks = array())
    {
        // line 87
        echo "          ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "profiles/social/themes/socialbase/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 87,  211 => 86,  205 => 120,  202 => 119,  195 => 115,  191 => 113,  189 => 112,  181 => 106,  175 => 103,  172 => 102,  170 => 101,  167 => 100,  160 => 96,  153 => 93,  151 => 92,  146 => 89,  144 => 86,  141 => 85,  135 => 83,  133 => 82,  129 => 81,  126 => 80,  120 => 77,  117 => 76,  115 => 75,  110 => 72,  104 => 69,  101 => 68,  99 => 67,  92 => 62,  87 => 59,  81 => 56,  78 => 55,  76 => 54,  71 => 52,  67 => 50,  65 => 49,  60 => 47,  53 => 43,  49 => 41,  47 => 37,  44 => 35,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Bartik's theme implementation to display a single page.*/
/*  **/
/*  * The doctype, html, head and body tags are not in this template. Instead they*/
/*  * can be found in the html.html.twig template normally located in the*/
/*  * core/modules/system directory.*/
/*  **/
/*  * Available variables:*/
/*  **/
/*  * General utility variables:*/
/*  * - base_path: The base URL path of the Drupal installation. Will usually be*/
/*  *   "/" unless you have installed Drupal in a sub-directory.*/
/*  * - is_front: A flag indicating if the current page is the front page.*/
/*  * - logged_in: A flag indicating if the user is registered and signed in.*/
/*  * - is_admin: A flag indicating if the user has permission to access*/
/*  *   administration pages.*/
/*  **/
/*  * Site identity:*/
/*  * - front_page: The URL of the front page. Use this instead of base_path when*/
/*  *   linking to the front page. This includes the language domain or prefix.*/
/*  **/
/*  * Page content (in order of occurrence in the default page.html.twig):*/
/*  * - node: Fully loaded node, if there is an automatically-loaded node*/
/*  *   associated with the page and the node ID is the second argument in the*/
/*  *   page's path (e.g. node/12345 and node/12345/revisions, but not*/
/*  *   comment/reply/12345).*/
/*  **/
/*  **/
/*  * @see template_preprocess_page()*/
/*  * @see html.html.twig*/
/*  *//* */
/* #}*/
/* */
/* {%*/
/*   set main_content_classes = [*/
/*     'region--main-content',*/
/*   ]*/
/* %}*/
/* */
/* <nav class="navbar navbar-default navbar-fixed-top" role="banner">*/
/*   <div class="container">{{ page.header }}</div>*/
/* </nav>*/
/* */
/* <main id="content" class="main-container" role="main">*/
/*   {{ page.breadcrumb }}*/
/* */
/*   {% if (page.hero or page.secondary_navigation) %}*/
/*     <div class="container hero-container">*/
/* */
/*       {{ page.hero }}*/
/* */
/*       {% if page.secondary_navigation %}*/
/*         <div class="brand-secondary z-depth-1">*/
/*           {{ page.secondary_navigation }}*/
/*         </div>*/
/*       {% endif %}*/
/* */
/*     </div>*/
/*   {% endif %}*/
/* */
/*   <a id="main-content" tabindex="-1"></a>*/
/* */
/*   <div class="container">*/
/* */
/*     {% if page.content_top %}*/
/*       <div class="margin-top-l">*/
/*         {{ page.content_top }}*/
/*       </div>*/
/*     {% endif %}*/
/* */
/*     <div class="row margin-top-l">*/
/* */
/*       {% if page.complementary_top or page.complementary_bottom %}*/
/*         <aside class="region--complementary complementary-top" role="complementary">*/
/*           {{ page.complementary_top }}*/
/*         </aside>*/
/*       {% endif %}*/
/* */
/*       <div{{ attributes.addClass(main_content_classes) }}">*/
/*         {% if page.title and display_page_title %}*/
/*           {{ page.title }}*/
/*         {% endif %}*/
/* */
/*         {% block content %}*/
/*           {{ page.content }}*/
/*         {% endblock %}*/
/* */
/*       </div>*/
/* */
/*       {% if page.complementary_bottom %}*/
/*       {{ attach_library('socialbase/responsive-dom') }}*/
/*         <div role="complementary" class="region--complementary">*/
/*           <div class="complementary-bottom">*/
/*             {{ page.complementary_bottom }}*/
/*           </div>*/
/*         </div>*/
/*       {% endif %}*/
/* */
/*       {% if page.content_bottom %}*/
/*         <div class="row margin-top-sm">*/
/*           {{ page.content_bottom }}*/
/*         </div>*/
/*       {% endif %}*/
/* */
/*     </div>*/
/*   </div>*/
/* */
/* </main>*/
/* */
/* {% if page.footer %}*/
/*   <footer class="site-footer" role="contentinfo">*/
/*     <div class="container clearfix">*/
/*       {{ page.footer }}*/
/*     </div>*/
/*   </footer>*/
/* {% endif %}*/
/* */
/* {{ svg_icons|raw }}*/
/* */
