<?php

/* profiles/social/themes/socialbase/templates/field/field.html.twig */
class __TwigTemplate_f91dbc8661c18c981921b7489d91fc2318cac12ef23466f90ca78deec85f09bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 41, "for" => 42, "set" => 57);
        $filters = array("striptags" => 45, "render" => 45, "clean_class" => 59);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if', 'for', 'set'),
                array('striptags', 'render', 'clean_class'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 41
        if ((isset($context["bare"]) ? $context["bare"] : null)) {
            // line 42
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 43
                echo "    ";
                if (((isset($context["entity_type"]) ? $context["entity_type"] : null) == "node")) {
                    // line 44
                    echo "      ";
                    if ((((isset($context["field_name"]) ? $context["field_name"] : null) == "body") && (isset($context["part_of_teaser"]) ? $context["part_of_teaser"] : null))) {
                        // line 45
                        echo "        ";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, strip_tags($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($context["item"], "content", array()))), "html", null, true));
                        echo "
      ";
                    } else {
                        // line 47
                        echo "        ";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "content", array()), "html", null, true));
                        echo "
      ";
                    }
                    // line 49
                    echo "    ";
                } else {
                    // line 50
                    echo "      ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "content", array()), "html", null, true));
                    echo "
    ";
                }
                // line 52
                echo "  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "
";
        } else {
            // line 55
            echo "
  ";
            // line 57
            $context["classes"] = array(0 => "field", 1 => ("field--name-" . \Drupal\Component\Utility\Html::getClass(            // line 59
(isset($context["field_name"]) ? $context["field_name"] : null))), 2 => ("field--type-" . \Drupal\Component\Utility\Html::getClass(            // line 60
(isset($context["field_type"]) ? $context["field_type"] : null))), 3 => ("field--label-" .             // line 61
(isset($context["label_display"]) ? $context["label_display"] : null)));
            // line 64
            echo "  ";
            // line 65
            $context["title_classes"] = array(0 => "field--label", 1 => (((            // line 67
(isset($context["label_display"]) ? $context["label_display"] : null) == "visually_hidden")) ? ("sr-only") : ("")));
            // line 70
            echo "

  ";
            // line 72
            if ((isset($context["label_hidden"]) ? $context["label_hidden"] : null)) {
                // line 73
                echo "    ";
                if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
                    // line 74
                    echo "      <div";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null), 1 => "field--items"), "method"), "html", null, true));
                    echo ">
        ";
                    // line 75
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 76
                        echo "          <div";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["item"], "attributes", array()), "addClass", array(0 => "field--item"), "method"), "html", null, true));
                        echo ">";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "content", array()), "html", null, true));
                        echo "</div>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 78
                    echo "      </div>
    ";
                } else {
                    // line 80
                    echo "      ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 81
                        echo "        <div";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null), 1 => "field--item"), "method"), "html", null, true));
                        echo ">";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "content", array()), "html", null, true));
                        echo "</div>
      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 83
                    echo "    ";
                }
                // line 84
                echo "  ";
            } else {
                // line 85
                echo "    <div";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method"), "html", null, true));
                echo ">
      <div";
                // line 86
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["title_attributes"]) ? $context["title_attributes"] : null), "addClass", array(0 => (isset($context["title_classes"]) ? $context["title_classes"] : null)), "method"), "html", null, true));
                echo ">";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true));
                echo "</div>
      ";
                // line 87
                if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
                    // line 88
                    echo "        <div class=\"field__items\">
      ";
                }
                // line 90
                echo "      ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 91
                    echo "        <div";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["item"], "attributes", array()), "addClass", array(0 => "field--item"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["item"], "content", array()), "html", null, true));
                    echo "</div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 93
                echo "      ";
                if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
                    // line 94
                    echo "        </div>
      ";
                }
                // line 96
                echo "    </div>
  ";
            }
            // line 98
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "profiles/social/themes/socialbase/templates/field/field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 98,  195 => 96,  191 => 94,  188 => 93,  177 => 91,  172 => 90,  168 => 88,  166 => 87,  160 => 86,  155 => 85,  152 => 84,  149 => 83,  138 => 81,  133 => 80,  129 => 78,  118 => 76,  114 => 75,  109 => 74,  106 => 73,  104 => 72,  100 => 70,  98 => 67,  97 => 65,  95 => 64,  93 => 61,  92 => 60,  91 => 59,  90 => 57,  87 => 55,  83 => 53,  77 => 52,  71 => 50,  68 => 49,  62 => 47,  56 => 45,  53 => 44,  50 => 43,  45 => 42,  43 => 41,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Theme override for a field.*/
/*  **/
/*  * To override output, copy the "field.html.twig" from the templates directory*/
/*  * to your theme's directory and customize it, just like customizing other*/
/*  * Drupal templates such as page.html.twig or node.html.twig.*/
/*  **/
/*  * Instead of overriding the theming for all fields, you can also just override*/
/*  * theming for a subset of fields using*/
/*  * @link themeable Theme hook suggestions. @endlink For example,*/
/*  * here are some theme hook suggestions that can be used for a field_foo field*/
/*  * on an article node type:*/
/*  * - field--node--field-foo--article.html.twig*/
/*  * - field--node--field-foo.html.twig*/
/*  * - field--node--article.html.twig*/
/*  * - field--field-foo.html.twig*/
/*  * - field--text-with-summary.html.twig*/
/*  * - field.html.twig*/
/*  **/
/*  * Available variables:*/
/*  * - attributes: HTML attributes for the containing element.*/
/*  * - label_hidden: Whether to show the field label or not.*/
/*  * - title_attributes: HTML attributes for the title.*/
/*  * - label: The label for the field.*/
/*  * - multiple: TRUE if a field can contain multiple items.*/
/*  * - items: List of all the field items. Each item contains:*/
/*  *   - attributes: List of HTML attributes for each item.*/
/*  *   - content: The field item's content.*/
/*  * - entity_type: The entity type to which the field belongs.*/
/*  * - field_name: The name of the field.*/
/*  * - field_type: The type of the field.*/
/*  * - label_display: The display settings for the label.*/
/*  **/
/*  **/
/*  * @see template_preprocess_field()*/
/*  *//* */
/* #}*/
/* {# Fields in the event teaser need to render without a div, because it is a child element of a tag that cannot have a div such as an anchor or paragraph #}*/
/* {% if bare %}*/
/*   {% for item in items %}*/
/*     {% if entity_type == "node" %}*/
/*       {% if field_name == "body" and part_of_teaser %}*/
/*         {{ item.content|render|striptags }}*/
/*       {% else %}*/
/*         {{ item.content }}*/
/*       {% endif %}*/
/*     {% else %}*/
/*       {{ item.content }}*/
/*     {% endif %}*/
/*   {% endfor %}*/
/* */
/* {% else %}*/
/* */
/*   {%*/
/*     set classes = [*/
/*       'field',*/
/*       'field--name-' ~ field_name|clean_class,*/
/*       'field--type-' ~ field_type|clean_class,*/
/*       'field--label-' ~ label_display,*/
/*     ]*/
/*   %}*/
/*   {%*/
/*     set title_classes = [*/
/*       'field--label',*/
/*       label_display == 'visually_hidden' ? 'sr-only',*/
/*     ]*/
/*   %}*/
/* */
/* */
/*   {% if label_hidden %}*/
/*     {% if multiple %}*/
/*       <div{{ attributes.addClass(classes, 'field--items') }}>*/
/*         {% for item in items %}*/
/*           <div{{ item.attributes.addClass('field--item') }}>{{ item.content }}</div>*/
/*         {% endfor %}*/
/*       </div>*/
/*     {% else %}*/
/*       {% for item in items %}*/
/*         <div{{ attributes.addClass(classes, 'field--item') }}>{{ item.content }}</div>*/
/*       {% endfor %}*/
/*     {% endif %}*/
/*   {% else %}*/
/*     <div{{ attributes.addClass(classes) }}>*/
/*       <div{{ title_attributes.addClass(title_classes) }}>{{ label }}</div>*/
/*       {% if multiple %}*/
/*         <div class="field__items">*/
/*       {% endif %}*/
/*       {% for item in items %}*/
/*         <div{{ item.attributes.addClass('field--item') }}>{{ item.content }}</div>*/
/*       {% endfor %}*/
/*       {% if multiple %}*/
/*         </div>*/
/*       {% endif %}*/
/*     </div>*/
/*   {% endif %}*/
/* */
/* {% endif %}*/
/* */
