<?php

/* node--teaser__field.html.twig */
class __TwigTemplate_225c3c48dd17bacaccdb20965d6bbd105917f6ab04a0bc2d3cea8132aee0e6fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("block" => 3);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"text-truncate\">
  <svg class=\"teaser-icon\">
      <use xlink:href=\"#icon-";
        // line 3
        $this->displayBlock('field_icon', $context, $blocks);
        echo "\"></use>
  </svg>
  <span class=\"inline-center\">
    ";
        // line 6
        $this->displayBlock('field_value', $context, $blocks);
        // line 7
        echo "  </span>
</div>
";
    }

    // line 3
    public function block_field_icon($context, array $blocks = array())
    {
        echo "label";
    }

    // line 6
    public function block_field_value($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "node--teaser__field.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  69 => 6,  63 => 3,  57 => 7,  55 => 6,  49 => 3,  45 => 1,);
    }
}
/* <div class="text-truncate">*/
/*   <svg class="teaser-icon">*/
/*       <use xlink:href="#icon-{% block field_icon %}label{% endblock %}"></use>*/
/*   </svg>*/
/*   <span class="inline-center">*/
/*     {% block field_value %} {% endblock %}*/
/*   </span>*/
/* </div>*/
/* */
